<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="jonan.*, java.util.ArrayList" %>
<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title>カート</title>
	</head>
	<body>
		<%	ArrayList<Item> cart = (ArrayList<Item>)session.getAttribute("cart");	%>
		<%	String warning = (String)request.getAttribute("warning");	%>
		<h1>カート</h1>
		<%-- カートが空のときの処理 --%>
		<%	if(cart == null || cart.isEmpty()) {	%>
			<p>カートは空です</p>
				
		<%-- カートの内容をループして表示 --%>
		<% } else {%>
			<%	if(warning != null && !warning.isEmpty()) {	%>
				<p><%= warning %><p>
			<%	}	%>
			<%	for(Item item:cart){	%>
				<div>
					<span><%= item.getName() %> <%= item.getPrice() %>円 <%= item.getStock() %>個</span>
					<form action="${pageContext.request.contextPath}/items" method="post" style="display: inline">
						<input type="hidden" name="id" value="<%= item.getId() %>">
						<input type="hidden" name="name" value="<%= item.getName() %>">
						<input type="hidden" name="price" value="<%= item.getPrice() %>">
						<input type="hidden" name="stock" value="<%= item.getStock() %>">
						<select name="quantity">
							<%-- ここにプルダウンメニューのオプションを追加 --%>
							<% for (int i = 1; i <= item.getStock(); i++) { %>
								<option value="<%= i %>" <% if(i==item.getQuantity())
								{ out.print("selected"); } %>><%= i %></option>
							<% } %>
						</select>
						<span>個</span>
						<input type="submit" name="cm" value="変更">
						<input type="submit" name="cm" value="商品を削除">
					</form>
				</div>
			<%	}	%>
		<%	}	%>
		<a href="${pageContext.request.contextPath}/items">商品詳細リストへ</a>
	</body>
</html>