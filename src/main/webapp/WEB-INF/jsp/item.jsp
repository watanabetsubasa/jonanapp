<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="jonan.Item, java.util.ArrayList" %>
<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title>商品詳細リスト</title>
	</head>
	<body>
		<%-- 商品リストを取得するロジックを追加 --%>
		<%	ArrayList<Item> itemList = (ArrayList<Item>)session.getAttribute("itemList");	%>
		<%	String warning = (String)request.getAttribute("warning");	%>
		<%	if(warning != null && !warning.isEmpty()) {	%>
				<p><%= warning %><p>
		<%	}	%>
		<h1>商品詳細リスト</h1>
		<%-- 商品リストをループして表示 --%>
		<%	for (Item item : itemList) {	%>
			<div>
				<span><%= item.getName() %> <%= item.getPrice() %>円 <%= item.getStock() %>個</span>
				<form action="${pageContext.request.contextPath}/items" method="post" style="display: inline">
					<input type="hidden" name="id" value="<%= item.getId() %>">
					<input type="hidden" name="name" value="<%= item.getName() %>">
					<input type="hidden" name="price" value="<%= item.getPrice() %>">
					<input type="submit" name="cm" value="カートに入れる">
					<select name="quantity">
						<%-- ここにプルダウンメニューのオプションを追加 --%>
						<% for (int i = 1; i <= item.getStock(); i++) { %>
							<option value="<%= i %>"><%= i %></option>
						<% } %>
					</select>
					<span>個</span>
				</form>
			</div>
		<% } %>
		<a href="${pageContext.request.contextPath}/cart">カート</a>
		<a href="${pageContext.request.contextPath}/login">ログイン</a>
	</body>
</html>