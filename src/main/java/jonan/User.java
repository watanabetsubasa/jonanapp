package jonan;

import java.io.Serializable;

public class User implements Serializable {
	private String id;
	private String name;
	
	public User(String id,String name){
		this.id=id;
		this.name=name;
	}
	
	public void setId(String id) {
		this.id=id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public String getName(String name) {
		return name;
	}
}
