package jonan;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//結果又はエラーのページに遷移
		String disp="WEB-INF/jsp/login.jsp";
		request.getRequestDispatcher(disp).forward(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String disp=null;
		
		String email = (String)request.getAttribute("email");
		String pass = (String)request.getAttribute("pass");
		User user=null;
		LoginLogic loginLogic=new LoginLogic();
		
		try {
			user=loginLogic.login(email, pass);
			if(user!=null) {
				request.setAttribute("warning", "ログインしました");
				disp="WEB-INF/jsp/login.jsp";
			}
		}catch (NullPointerException | NumberFormatException | SQLException |IllegalStateException e){
			request.setAttribute("errorMessage", e.getMessage());
			disp="WEB-INF/jsp/error.jsp";
		}
		
		//結果又はエラーのページに遷移
		request.getRequestDispatcher(disp).forward(request,response);
	}



}
