package jonan;

import java.io.Serializable;

public class Item implements Serializable {
	private int id;
	private String name;
	private int price;
	private int stock;
	private int quantity;
	
	public Item(int id,String name,int price,int stock){
		this.id=id;
		this.name=name;
		this.price=price;
		this.stock=stock;
		this.quantity=0;
	}
	
	public void setQuantity(int quantity){
		this.quantity=quantity;
	}
	
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public int getPrice(){
		return price;
	}
	
	public int getStock(){
		return stock;
	}
	
	public int getQuantity(){
		return quantity;
	}
	
	public String toString(){
		return name + " " + price + " " + quantity;
	}

}


