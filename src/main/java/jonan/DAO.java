package jonan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAO {
	public ArrayList<Item> list() throws SQLException,ClassNotFoundException {
		ArrayList<Item> itemList = new ArrayList<>();
		// JDBCドライバを読み込む
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("JDBCドライバを読み込めませんでした");
		}	
		
		// データベースに接続
		try(Connection cn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/jonan", "sa", "")){
			PreparedStatement ps = cn.prepareStatement("select id,name,price,stock from items");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				Item item = new Item(rs.getInt("id"),rs.getString("name"),rs.getInt("price"),rs.getInt("stock"));
				itemList.add(item);
			}
			
			if (itemList.isEmpty()) {
				throw new SQLException();
			}
			
		}catch (SQLException e) {
			throw new SQLException(e.getMessage());
		}

		return itemList;
	}
	
	public Item search(int id) throws SQLException,ClassNotFoundException {
		Item item = null;
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("JDBCドライバを読み込めませんでした");
		}	
		
		try(Connection cn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/jonan", "sa", "")){
			PreparedStatement ps = cn.prepareStatement("select id,name,price,stock from items where id= ?");
			ps.setInt(1,id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				item = new Item(rs.getInt("id"),rs.getString("name"),rs.getInt("price"),rs.getInt("stock"));
			}
			if (item == null) {
				throw new SQLException();
			}
		}catch (NullPointerException e){
			throw new SQLException(id+ "のitemのデータが見つかりませんでした。");
		}catch (SQLException e) {
			throw new SQLException(id+ "のitemのデータが見つかりませんでした。");
		}
		
		return item;
	}
}


