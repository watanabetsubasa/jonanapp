package jonan;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/items")
public class ItemsServlet extends HttpServlet {
	public void doGet(HttpServletRequest request,HttpServletResponse response)
	throws ServletException, IOException {
		ArrayList<Item> itemList = new ArrayList<>();
		DAO dao = new DAO();
		HttpSession session =request.getSession();
		String disp="";
		try{
			//商品詳細を表示する処理
			itemList = dao.list();
			session.setAttribute("itemList",itemList);
			disp="WEB-INF/jsp/item.jsp";
		}catch(NumberFormatException e){
			request.setAttribute("errorMessage", "数字を入力してください。");
			disp="WEB-INF/jsp/error.jsp";
		}catch(SQLException | ClassNotFoundException e){
			request.setAttribute("errorMessage", e.getMessage());
			disp="WEB-INF/jsp/error.jsp";
		}
		
		//結果又はエラーのページに遷移
		request.getRequestDispatcher(disp).forward(request,response);
	}
	
	
	public void doPost (HttpServletRequest request,HttpServletResponse response)
	throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8");
		request.setCharacterEncoding("UTF-8");
		DAO dao = new DAO();
		HttpSession session =request.getSession();
		ArrayList<Item> cart = new ArrayList<>(); 
		Item item = null;
		
		//cartのセッションがない、または空でないときはセッションをカート配列リストに入れる。
		if(session.getAttribute("cart") != null && !((ArrayList<Item>)session.getAttribute("cart")).isEmpty()){
			cart = (ArrayList<Item>)session.getAttribute("cart");//これで注意が出る
		}
		String disp="";
		
		//formから命令の種類を受け取る
		String cm = request.getParameter("cm");
		try{
			//カートに入れるコード
			if(cm.equals("カートに入れる")){
					int id = Integer.parseInt(request.getParameter("id"));
					int quantity = Integer.parseInt(request.getParameter("quantity"));
					item = dao.search(id);
					boolean itemExists = false;
					if(quantity>0){
						// cart内の要素を確認して指定された条件に応じて処理を行う
						for(int i=0;i<cart.size();i++){
							if(cart.get(i).getId() == item.getId()){
								// もし指定したidのItemがすでにcart内に存在する場合は上書き
								int stock = cart.get(i).getStock();
								int preQ = cart.get(i).getQuantity();
								//既に入っている個数と追加する個数の和と、在庫数に応じて処理を分ける
								if(preQ+quantity<=stock){
									cart.get(i).setQuantity(preQ+quantity);
								}else{
									cart.get(i).setQuantity(stock);
									request.setAttribute("warning", "在庫数以上の個数をカートに入れることはできません。");
								}
								itemExists = true;
								break;
							}
						}
						if (!itemExists) {
							int stock = item.getStock();
							if(quantity<=stock){
								// cart内に指定したidのItemが存在しない場合は新しいItemを追加
								item.setQuantity(quantity);
								cart.add(item);
							}else{
								item.setQuantity(stock);
								cart.add(item);
								request.setAttribute("warning", "在庫数以上の個数をカートに入れることはできません。");
							}
						}
					session.setAttribute("cart",cart);
					disp="WEB-INF/jsp/cart.jsp";
					}else{
						//正の数でない数を送信されたとき
						request.setAttribute("warning", "正しい数字を選択してください。");
						disp="WEB-INF/jsp/item.jsp";
					}
					
			//カートの内容を変更するコード
			}else if(cm.equals("変更")){
					int id = Integer.parseInt(request.getParameter("id"));
					int quantity = Integer.parseInt(request.getParameter("quantity"));
					Item changeItem = dao.search(id);
					if(quantity>0){
						// cart内の要素を確認して指定したidに応じて処理を行う
						for(int i=0; i< cart.size(); i++){
							//上書き
							int stock = cart.get(i).getStock();
							if(cart.get(i).getId() == changeItem.getId()){
								if(quantity<=stock){
									cart.get(i).setQuantity(quantity); //数量を更新
								}else{
									//formのvalueを書き換えられた場合のコード
									cart.get(i).setQuantity(stock); //数量を在庫数に更新
									request.setAttribute("warning","在庫数以上の個数をカートに入れることはできません。");
								}
								break;
							}
						}
						// セッションに更新されたcartを保存
						session.setAttribute("cart",cart);
					}else{
						//formに非正数が入力された場合
						request.setAttribute("warning","正しい数字を選択してください。");
					}
					disp="WEB-INF/jsp/cart.jsp";
			//カートの内容を削除するコード
			}else if(cm.equals("商品を削除")){
				int id = Integer.parseInt(request.getParameter("id"));
				cart.removeIf(c -> c.getId() == id);
				session.setAttribute("cart",cart);
				disp="WEB-INF/jsp/cart.jsp";
				
			//想定外の命令時にエラーを表示
			}else{
				request.setAttribute("errorMessage", "想定外の命令を検知");
				disp="WEB-INF/jsp/error.jsp";
			}
			
		}catch(NullPointerException e){
			request.setAttribute("errorMessage", "ヌルポ");
			disp="WEB-INF/jsp/error.jsp";
		}catch(NumberFormatException e){
			request.setAttribute("errorMessage", "正しい数字を選択してください。");
			disp="WEB-INF/jsp/error.jsp";
		}catch(SQLException | ClassNotFoundException e){
			request.setAttribute("errorMessage", e.getMessage());
			disp="WEB-INF/jsp/error.jsp";
		}
		
		//結果又はエラーのページに遷移
		request.getRequestDispatcher(disp).forward(request,response);
	}
}
