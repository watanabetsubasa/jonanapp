package jonan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginLogic {
	public User login(String email,String pass) throws SQLException {
		User user=null;
		// JDBCドライバを読み込む
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("JDBCドライバを読み込めませんでした");
		}
		
		// データベースに接続
		try(Connection cn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/capybara", "sa", "")){
			PreparedStatement ps = cn.prepareStatement("select id,name from users where email = ? and pass=?");
			ps.setString(1, email);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				user= new User(rs.getString("id"),rs.getString("name"));
			}
			
		}catch (SQLException e) {
			throw new SQLException(e.getMessage());
		}

		
		return user;
	}
}
